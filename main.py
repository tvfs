#!/usr/bin/python

# tvfs - a simple fuse-module to mount a tv-guide as a filesystem
# Copyright (C) 2008,  Kristian Rumberg (kristianrumberg@gmail.com)

# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.

# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import re
import urllib
import tempfile
import gzip
import libxml2
import datetime

class Error(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value
    def __str__(self):
        return repr(self.value)

class GZStream:
    def __init__(self, url):
        self.url = url

    def read(self):
        stream = urllib.urlopen(self.url)
        buff = stream.read()
        f = tempfile.NamedTemporaryFile()
        f.write(buff)
        f.flush()
        gs = gzip.open(f.name)
        buff = gs.read()
        gs.close()
        f.close()
        return buff

class Time:
    def __init__(self, timestr):
        o = re.match("([0-9][0-9]):([0-9][0-9])", timestr)
        if None == o:
            raise Error("Unable to parse time string")
        self.hour   = str(o.group(1))
        self.minute = str(o.group(2))

    def __str__(self):
        return self.hour + ":" + self.minute

class Program:
    def __init__(self, name, desc, start_str, end_str):
        self.name  = name
        self.desc  = desc
        self.start = Time(start_str)
        self.end   = Time(end_str)

    def get_name(self):
        return self.name

    def get_description(self):
        return self.desc

    def get_start(self):
        return self.start

    def get_end(self):
        return self.end

class ProgramList:
    def __init__(self, name, url):
        self.url  = url
        self.name = name
        self.cl   = []

        istream = GZStream(url)
        doc = libxml2.parseDoc(istream.read())

        namelist    = doc.xpathEval('//programme//title/text()')
        desclist    = doc.xpathEval('//programme//desc/text()')
        startlist   = doc.xpathEval('//programme/attribute::start')
        endlist     = doc.xpathEval('//programme/attribute::stop')

        for name,desc,start,end in zip(namelist,desclist,startlist,endlist):
            name  = str(name.content)
            desc  = str(desc.content)

            start = str(start.content)[8:12]
            start = start[0:2] + ":" + start[2:4]

            end   = str(end.content)[8:12]
            end   = end[0:2] + ":" + end[2:4]

            self.cl.append( Program(name, desc, start, end) )

    def __iter__(self):
        return self.cl.__iter__()

class Channel:
    def __init__(self, name, baseurl):
        self.name = name
        self.baseurl = baseurl
        self.today = None

    def list_today(self):
        if None == self.today:
            d = datetime.datetime.now()
            path = self.baseurl + "_" + d.strftime("%Y-%m-%d") + ".xml.gz"
            self.today = ProgramList(self.name, path)
        return self.today

    def get_name(self):
        return self.name

class ChannelList:
    def __init__(self):
        basepath = "http://tv.swedb.se/xmltv/"
        self.cl = []

        istream = GZStream(basepath + "channels.xml.gz")
        doc = libxml2.parseDoc(istream.read())

        namelist    = doc.xpathEval('//display-name/text()')
        idlist      = doc.xpathEval('//channel/attribute::id')
        baselist    = doc.xpathEval('//base-url/text()')

        for name,idn,base in zip(namelist,idlist,baselist):
            name     = str(name.content)
            idn      = str(idn.content)
            base     = str(base.content)
            self.cl.append( Channel(name, base + idn))

    def __iter__(self):
        return self.cl.__iter__()

    def has_channel(self, chanstr):
        return -1 != self.cl.index[chanstr]


def main():
    cl = ChannelList()
    for c in cl:
        print c.get_name()
        pl = c.list_today()
        for p in pl:
            print p.get_name()
            print p.get_start()
            print p.get_end()
            print p.get_description()

if __name__ == "__main__":
    main()

